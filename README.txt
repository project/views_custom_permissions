CONTENTS OF THIS FILE
---------------------

 * Introduction 
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module allows users to create custom access callback(permissions) to views.

INSTALLATION
------------

 * Install the Views Custom Permissions module as you would normally install a 
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 
   for further information.    

CONFIGURATION
-------------

  -> Go to "/admin/config/system/views-custom-permissions" and add Title and Access callback function name.

      Example :

      PERMISSION(TITLE) : Manager
      CALLBACK(ACCESS CALLBACK FUCTION NAME) : accessCallback_ManagerRole    


  -> Create Access Callback function in the module file.

       Example :

         use Drupal\Core\Access\AccessResult;

         function accessCallback_ManagerRole() {
           $access = FALSE;
           if(// Some logic) {
             $access = TRUE;
           }
           // Return object should be type of AccessResult.
           return AccessResult::allowedIf($access);
         }
  

MAINTAINERS
-----------

Current maintainers:
  Elavarasan R - https://www.drupal.org/user/1902634
